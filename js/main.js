jQuery(document).ready(function($) {

    "use strict";
    var OnePageNavigation = function() {
      var navToggler = $('.site-menu-toggle');
       $("body").on("click", ".easing", function(e) {
        e.preventDefault();
  
        var hash = this.hash;
  
        $('html, body').animate({
          'scrollTop': $(hash).offset().top
        }, 600, 'easeInOutExpo', function(){
          window.location.hash = hash;
        });
  
      });
    };
    OnePageNavigation();
})